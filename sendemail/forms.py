from django import forms
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import send_mail, BadHeaderError


class ContactForm(forms.Form):
    name = forms.CharField(max_length=80, label='Name')
    email = forms.EmailField(required=False, label='Email Address')
    number = forms.CharField(max_length=20, label='Contact Number')
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)


def contact(request):
    submitted = False
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = f'JF97 Inquiry: {form.cleaned_data["name"]} / {form.cleaned_data["number"]}'
            message = form.cleaned_data["message"]
            sender = form.cleaned_data["email"]
            recipients = ["j.enricobandong@gmail.com"]
            try:
                send_mail(subject, message, sender,
                          recipients, fail_silently=True)
            except BadHeaderError:
                return HttpResponse('Invalid header found')
            return HttpResponseRedirect('/contact?submitted=True')

    else:
        form = ContactForm()
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'dist/contact.html', {'form': form, 'submitted': submitted})
