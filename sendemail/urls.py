from django.contrib import admin
from django.urls import path
from . import forms


urlpatterns = [
    path('', forms.contact, name='contact'),
]
