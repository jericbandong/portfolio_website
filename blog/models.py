from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
from . fields import NonStrippingTextField


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to="blog/", null=True)
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    body = NonStrippingTextField()
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.title

    def summary(self):
        return self.body[:200] + '...'

    def get_absolute_url(self):
        return reverse('post_detail', args=[str(self.id)])


def slug_generator(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(slug_generator, sender=Post)