# Generated by Django 3.0.4 on 2020-03-21 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20200321_1626'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='author',
        ),
        migrations.AddField(
            model_name='post',
            name='auth.User',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
