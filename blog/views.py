from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from . models import Post


# Home page
class HomePageView(TemplateView):
    template_name = 'dist/index.html'


# About page
class AboutPageView(TemplateView):
    template_name = 'dist/about.html'


# Experience page
class ExperiencePageView(TemplateView):
    template_name = 'dist/experience.html'


# Contact page
class ContactPageView(TemplateView):
    template_name = 'dist/contact.html'


# Blog list with search function
# def blog_search_list_view(request):
#     query = request.GET.get('q', None)
#     qs = Post.objects.all()
#     if query is not None:
#         qs = qs.filter(
#             Q(title__icontains=query) |
#             Q(slug__icontains=query) |
#             Q(content__icontains=query)
#         )
#     context = {
#         "posts": qs,
#     }
#     template = "dist/blog.html"
#     return render(request, template, context)

# class SearchResultsListView(ListView):
#     model = Post
#     template_name = 'dist/blog_search.html'
#     ordering = ['-added']
#     context_object_name = 'posts'
#     paginate_by = 4

#     def get_queryset(self):
#         query = self.request.GET.get('q')
#         return Post.objects.filter(
#             Q(title__icontains=query)
#         )

class BlogListView(ListView):
    model = Post
    template_name = 'dist/blog.html'
    ordering = ['-added']
    context_object_name = 'posts'
    paginate_by = 4


class BlogDetailView(DetailView):
    model = Post
    template_name = 'dist/blog_detail.html'

    def get_object(self):
        return get_object_or_404(Post, slug=self.kwargs['slug_title'])

# def detail(request, slug_title):
#     q = Post.objects.filter(slug=slug_title)
#     if q.exists():
#         q = q.first()
#     else:
#         return HttResponse("<h1>Page not found</h1>")

#     context = {
#         'posts': q,
#     }
#     return render(request, 'dist/blog_detail.html', context)


class BlogCreateView(CreateView):
    model = Post
    template_name = 'dist/blog_new.html'
    fields = '__all__'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        # return http.HttpResponseRedirect(self.get_success_url())
        return HttpResponseRedirect('/blog?submitted=True')


class BlogUpdateView(UpdateView):
    model = Post
    template_name = 'dist/blog_edit.html'
    fields = ['title', 'body']
    success_url = reverse_lazy('blog')

    # def form_valid(self, form):
    #     self.object = form.save(commit=False)
    #     self.object.save()
    #     # return HttpResponseRedirect(self.get_success_url())
    #     return HttpResponseRedirect('?submitted=True')
        

    # def post(self, request, *args, **kwargs):
    #     form = self.form_class(request.POST)
    #     if form.is_valid():
    #         # <process form cleaned data>
    #         return HttpResponseRedirect('?submitted=True')

    #     return render(request, self.template_name, {'form': form})

    def get_object(self):
        return get_object_or_404(Post, slug=self.kwargs['slug_title'])


class BlogDeleteView(DeleteView):
    model = Post
    template_name = 'dist/blog_delete.html'
    success_url = reverse_lazy('blog')

    def get_object(self):
        return get_object_or_404(Post, slug=self.kwargs['slug_title'])
