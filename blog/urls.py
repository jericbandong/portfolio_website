from django.urls import path, re_path
from django.conf.urls import url
from . import views


urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('about', views.AboutPageView.as_view(), name='about'),
    path('experience', views.ExperiencePageView.as_view(), name='experience'),
#     path('blog/search/', views.SearchResultsListView.as_view(), name='search_results'),
    path('blog', views.BlogListView.as_view(), name='blog'),
#     path('blog', views.blog_search_list_view, name='blog'),
    path('blog/post/<slug:slug_title>/', views.BlogDetailView.as_view(), name='post_detail'),
#    path('blog/post/<slug:slug_title>', views.detail, name="post_detail"),
    path('blog/new/', views.BlogCreateView.as_view(), name='post_new'),
#     re_path(r'blog/?P<post>.*/edit/', views.BlogUpdateView.as_view(), name='post_edit'),
    path('blog/post/<slug:slug_title>/edit/', views.BlogUpdateView.as_view(), name='post_edit'),
#     url(r'^blog/post/(.*)/edit/$', views.BlogUpdateView.as_view(), name='post_edit'),
    path('blog/post/<slug:slug_title>/delete/', views.BlogDeleteView.as_view(), name='post_delete'),
    
]
